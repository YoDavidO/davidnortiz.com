<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'davidnortiz');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '64b:83m1&8Fd2~1jmM`i%O|z9(]J!l?,BX{-}5@1#.To|S65jhlqLE_ vVD^G9XP');
define('SECURE_AUTH_KEY',  '`oe1P/r$>e&Z7;%G}~^E72I+TgCPRU#kYb.a<0-{Q_N-`3!k.fIfl.Gn${BPfmor');
define('LOGGED_IN_KEY',    'ylsp8|pQlbEDn5lHQ4YDzqn<L`[Kp+i<.*|(1dQO-lOXh2})W $:u~uu+CAR8HXy');
define('NONCE_KEY',        'w}h1kZ)23Rzm+`#,KC;|W~a5Y@i0?]J_3.*)uZqlhWk9W)){jG/?EQlO7>9!,E:,');
define('AUTH_SALT',        'T71d-|~LY[nle|$:-:GJYYUd]/wf=>:&q2ADDQjmVYAZJhn]sy]H3].puL4W;ivz');
define('SECURE_AUTH_SALT', 'eI#5S=&O^r.C-?dHu}+[L{T,FVia!U8cKY(g75.H&X-+EN-*H1]:Bmuo{e>BwJj7');
define('LOGGED_IN_SALT',   'OIom!w|!,iAG#L;N&s=ASa0CW|jZ#0nf%BO?H3M?t^M;JbwtQOp=y[<]q60T_7eH');
define('NONCE_SALT',       'm_NyPNfZz@Fa=)HT+:K>|w9F?c8^G^1(NPV *SXB8 K+t%v*(@Txl|a x8{b`#|c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
