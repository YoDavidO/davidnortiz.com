<?php
	
function dno_scripts()
{
	wp_enqueue_script( 'jquery' );

	wp_register_script(
		'vendor',
		get_stylesheet_directory_uri() . '/scripts/vendor.min.js',
		array( 'jquery' )
	);

	wp_enqueue_script(
		'dno_scripts',
		get_stylesheet_directory_uri() . '/scripts/site.min.js',
		array( 'jquery', 'vendor' )
	);

	wp_enqueue_style( 'fontStyles', get_template_directory_uri() . '/styles/fontello.css' ); 

	wp_enqueue_style( 'styles', 
		get_stylesheet_uri(),
		array( 'fontStyles' ) 
	);

	wp_localize_script(
		'dno_scripts',
		'myLocalized',
		array( 
			'partials' => trailingslashit( get_template_directory_uri() ) . 'partials/',
			'images' => trailingslashit( get_template_directory_uri() ) . 'images/'
		)
	);
}

add_action( 'wp_enqueue_scripts', 'dno_scripts' );


add_filter('json_prepare_post', 'json_api_encode_acf');

function json_api_encode_acf($post) {
    
    $acf = get_fields($post['ID']);
    
    if (isset($post)) {
      $post['acf'] = $acf;
    }

    return $post;

}

function allow_my_post_types($allowed_post_types) {
	$allowed_post_types[] = 'Projects';
	return $allowed_post_types;
}

add_filter( 'rest_api_allowed_post_types', 'allow_my_post_types');
















