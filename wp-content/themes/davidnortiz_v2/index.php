<!DOCTYPE html>
<html ng-app="DavidOrtiz" style="margin-top : 0px !important;">
<head>
    <base href="/">
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, maximum-scale=1, initial-scale=1, minimum-scale=1">
    <title>David N. Ortiz :: Developer :: Super Stealthy Ninja</title>
    <?php wp_head(); ?>
</head>

<body ng-controller="LayoutCtrl as layout">

    <m-nav close-menu="{{layout.closeMenu}}"></m-nav>
    
    <header class="header">
        <div id="primary-navigation">
            <a class="navLogo"><img src="{{layout.navLogo}}" alt=""/></a>
            <ul>
                <li><a href="#/projects" title="" ng-class="{true : 'currentSection'}[layout.currentSection === 'projects']">PROJECTS</a></li>
                <li><a href="#/profile" title="" ng-class="{true : 'currentSection'}[layout.currentSection === 'profile']">PROFILE</a></li>
                <li><a href="#/contact" title="" ng-class="{true : 'currentSection'}[layout.currentSection === 'contact']">CONTACT</a></li>
            </ul>
        </div>
    </header>

    <div class="uiViewPort" ui-view></div>


    <footer class="footer" ng-class="{'hideMe':layout.hideFooter}">
        <img src="{{layout.navLogo}}" alt="">
        <hr>
        <ul>
            <li><a href="mailto:david@davidnortiz.com">EMAIL ME</a></li>
            <li><a href="skype:yodavido?call">SKYPE ME</a></li>
            <li><a href="http://twitter.com/yodavido">TWEET ME</a></li>
            <li><a href="http://instagram.com/yodavido">FOLLOW ME</a></li>
        </ul>
    </footer>

</body>

</html>