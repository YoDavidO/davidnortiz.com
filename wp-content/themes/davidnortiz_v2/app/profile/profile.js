( function(){

	angular.module('DavidOrtiz.profile', [ 'DavidOrtiz.services', 'ngSanitize' ] )
		.controller('ProfileCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'pages',
			profileController
		] );

	function profileController( $rootScope, $scope, $timeout, $stateParams, pages )
	{
		var me = this;

		me.profileImg = myLocalized.images + '/profilePic.png'; 

		pages.getPages().$promise.then( function( res ){
			$timeout( function(){
				me.profileBody = res[0].content;
			} )
		});
	}

})();