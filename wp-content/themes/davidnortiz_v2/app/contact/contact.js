(function(){

	angular.module('DavidOrtiz.contact', [])
		.controller( 'ContactCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			contactController
		] );

	function contactController( $rootScope, $scope, $timeout, $stateParams )
	{

		var me = this;

		me.skypeImg = myLocalized.images + 'skype_contact.png';
		me.skypeImg_over = myLocalized.images + 'skype_contact_green.png';
		me.instagramImg = myLocalized.images + 'instagram_contact.png';
		me.instagramImg_over = myLocalized.images + 'instagram_contact_green.png';
		me.twitterImg = myLocalized.images + 'twitter_contact.png';
		me.twitterImg_over = myLocalized.images + 'twitter_contact_green.png';

		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});
	}

})();