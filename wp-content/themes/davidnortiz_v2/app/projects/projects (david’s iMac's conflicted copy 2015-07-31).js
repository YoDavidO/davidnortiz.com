( function(){

	angular.module('DavidOrtiz.projects', [ 'DavidOrtiz.services' ] )
		.controller('ProjectsCtrl', [
			'$rootScope',
			'$scope', 
			'$timeout',
			'$stateParams',
			'projectsList',
			projectsController
		] )
		.directive('projectTile', [function () {
			return { restrict: 'A', link: projectTileDirective };
		}]);

	function projectsController( $rootScope, $scope, $timeout, $stateParams, projectsList )
	{
		var projectsScope = this;
		projectsScope.projects = [];
		projectsList.getProjects().$promise.then( function( res ){
			for( var i = 0; i < res.length; i++ )
			{
				projectsScope.projects.push( res[i] );
			}
		});

		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});

	}

	function projectTileDirective( scope, element, attr )
	{
		var win = jQuery( window );
		element.bind( 'mouseover', function( e ){
			TweenMax.to( element.find( '.projectTileOverlay' ), .35, { opacity : 1, ease : Quad.easeOut, overwrite : 'all' } );
		});
		element.bind( 'mouseout', function( e ){
			TweenMax.to( element.find( '.projectTileOverlay' ), .35, { opacity : 0, ease : Quad.easeOut } );
		});
	}

})();