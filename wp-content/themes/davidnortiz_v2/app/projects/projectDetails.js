(function(){

	angular.module( 'DavidOrtiz.projectDetails', [ 'DavidOrtiz.services' ] )
		.controller('ProjectDetailsCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'$filter',
			'projectsList',
			projectDetailsController
		])
		.controller('MProjectDetailsCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'$filter',
			'projectsList',
			mProjectDetailsController
		])
		.directive( 'detailOpener', [ function(){
			return {
				restrict : 'A',
				link : function( scope, element, attrs )
				{
					var detailsOpened = false;

					element.bind( 'click', function( e ){
						e.preventDefault();

						if( detailsOpened )
						{
							detailsOpened = false;
							element.removeClass('open');
						}
						else
						{
							detailsOpened = true;
							element.addClass('open');
						}
					})
				}
			};
		}]);


	function projectDetailsController( $rootScope, $scope, $timeout, $stateParams, $filter, projectsList )
	{
		var pdcScope = this;
		pdcScope.project = {};

		pdcScope.detailsOpen = myLocalized.images + 'leftArrow.png';

		projectsList.getProjects().$promise.then( function( res ){
			$timeout( function(){
				pdcScope.project = $filter('filter')( res, { ID : parseInt( $stateParams.id ) } )[0];
				pdcScope.primaryImage = pdcScope.project.acf.project_images[0].url;

				if( pdcScope.project.acf.project_link === "" )
					pdcScope.noLink = true;
				else
					pdcScope.noLink = false;

				$rootScope.currentSection = $stateParams.sectionTitle;
			});
		});
	}


	function mProjectDetailsController( $rootScope, $scope, $timeout, $stateParams, $filter, projectsList )
	{
		$scope.project = {};
		projectsList.getProjects().$promise.then( function( res ){
			$timeout( function(){
				$scope.project = $filter( 'filter' )( res, { ID : parseInt( $stateParams.id ) } )[0];
			});
		});
		$rootScope.$on( '$stateChangeStart', function( e, to ){
			projectsList.getProjects().$promise.then( function( res ){
				$timeout( function(){
					$scope.project = $filter( 'filter' )( res, { ID : parseInt( $stateParams.id ) } )[0];
				});
			});
		});
		
	}

})();















