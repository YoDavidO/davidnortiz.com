(function(){
	angular.module( 'DavidOrtiz.intro', [] )
		.controller( 'IntroCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			introController
		]);

	function introController( $rootScope, $scope, $timeout, $stateParams )
	{
		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});
	}
})();