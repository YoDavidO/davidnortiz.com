(function(){
	app = angular.module( 'DavidOrtiz', [
        'ngResource',
        'ngSanitize',
        'ui.router',
        'DavidOrtiz.services',
        'DavidOrtiz.directives',
        'DavidOrtiz.filters',
		'DavidOrtiz.layout',
        'DavidOrtiz.intro',
		'DavidOrtiz.projects',
        'DavidOrtiz.projectDetails',
		'DavidOrtiz.profile',
		'DavidOrtiz.contact'
	])
	.config([
		'$stateProvider', 
        '$urlRouterProvider',
		function ($stateProvider, $urlRouterProvider) 
		{
            $urlRouterProvider.otherwise( function($injector){
                $state = $injector.get( '$state' );
                $state.go( 'projects' );
            } )

			$stateProvider
                .state( 'projects', {
                    url : '/projects',
                    templateUrl : myLocalized.partials + '/projects.html',
                    params : {
                        sectionTitle : 'PROJECTS'
                    }
                })
                .state( 'projects_direct', {
                    url : '/projects/:id',
                    templateUrl : myLocalized.partials + '/projectDetails.html',
                    params : {
                        sectionTitle : 'PROJECTS'
                    }
                })
                .state( 'profile', {
                    url : '/profile',
                    templateUrl : myLocalized.partials + '/profile.html',
                    params : {
                        sectionTitle : 'PROFILE'
                    }
                })
                .state( 'contact', {
                    url : '/contact',
                    templateUrl : myLocalized.partials + '/contact.html',
                    params : {
                        sectionTitle : 'CONTACT'
                    }
                });
		}
	]);
})();
