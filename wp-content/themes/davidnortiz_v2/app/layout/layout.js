( function(){

	angular.module('DavidOrtiz.layout', [] )
		.controller('LayoutCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			layoutController
		] );

	function layoutController( $rootScope, $scope, $timeout )
	{
		var layout = this;
		layout.navLogo = myLocalized.images + 'pastelLogo.png';
        layout.dNav = myLocalized.partials = '/nav.html';

        $rootScope.$on( '$stateChangeStart', function(e, to){
        	$timeout(function(){
				layout.currentSection = to.name;
				if( layout.closeMenu )
					layout.closeMenu = false;
				else
					layout.closeMenu = true;

				if( layout.currentSection === 'contact' )
					layout.hideFooter = true;
				else
					layout.hideFooter = false;
			});
        });
		
	}

})();