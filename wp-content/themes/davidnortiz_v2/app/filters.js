(function(){

	filters = angular.module('DavidOrtiz.filters', []);

	filters.filter( 'htmlToPlainText', function(){
		return function( text ){
			return String(text).replace(/<[^>]+>/gm, '').trim();
		}
	})

})();