(function(){
	
	directives = angular.module('DavidOrtiz.directives', [] );

	directives.directive('loadingSection', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                iElement.hide();
                TweenMax.to( iElement, 0, { opacity : 0 } );
                $rootScope.$on( '$viewContentLoaded', function(e){
                    iElement.show();
                    TweenMax.to( iElement, 0.35, { opacity : 1, ease : Quad.easeOut } );
                } );
            }
        };
    }]);

    directives.directive('loadingImg', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                var $superParent = iElement.parent().parent().parent();
                $superParent.hide();
                TweenMax.to( $superParent, 0, { opacity : 0 } );
                iElement.on( 'load', function(e){
                    $superParent.show();
                    TweenMax.to( $superParent, 1, { opacity : 1, ease : Expo.easeInOut} );
                });
            }
        };
    }]);

    directives.directive('mNav', [function () {
        return {
            restrict: 'E',
            link: function (scope, iElement, iAttrs) {
                var toggles = iElement.find('button');
                var mMenu = iElement.find( '.mnav' );
                var menuOpen = false;
                var navBtns = iElement.find( 'a' );

                TweenMax.to( mMenu, 0, { opacity : 0 } );
                toggles.on( "click", function(e) {
                    e.preventDefault();
                    (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
                    toggler();
                });

                navBtns.on( 'click', function( e ){
                    toggles.removeClass( 'is-active' );
                    menuOpen = false;
                    TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                        mMenu.hide();
                    } } );

                    navBtns.removeClass( 'mCurrentSection' );
                    jQuery( e.currentTarget ).addClass('mCurrentSection');
                });

                function toggler()
                {
                    if( menuOpen )
                    {
                        menuOpen = false;
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                    else
                    {
                        menuOpen = true;
                        mMenu.show();
                        TweenMax.to( mMenu, .5, { opacity : 1, ease : Expo.easeOut, overwrite : 'all' } );
                    }
                }

                iAttrs.$observe( 'closeMenu', function( res ){
                    if( menuOpen )
                    {
                        menuOpen = false;
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                } );

                var win = jQuery( window );
                win.on( 'resize', function(){
                    if( win.width() > 960 && menuOpen )
                    {
                        menuOpen = false;
                        toggles.removeClass( 'is-active' );
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                })
            },
            templateUrl : myLocalized.partials + "/mNav.html"
        };
    }]);

    directives.directive('btnOverer', [function () {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                var out = iElement.find( '.contactBtn' );
                var over = iElement.find( '.contactBtn_over' );

                over.show();
                TweenMax.to( over, 0, { opacity : 0 } );

                iElement.on( 'mouseenter', function(e){
                    TweenMax.to( over, 0.35, { opacity : 1, ease : Quad.easeOut, overwrite : 'all' } );
                });
                iElement.on( 'mouseout', function(e){
                    TweenMax.to( over, 0.35, { opacity : 0, ease : Quad.easeOut, overwrite : 'all' } );
                });
            }
        };
    }]);
})();






