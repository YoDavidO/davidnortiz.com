(function(){

	services = angular.module('DavidOrtiz.services', []);

	services.factory('projectsList', [
		'$resource',
		function ( $resource ) 
		{
			projects = {};

			projects.getProjects = function()
			{
				var projectsList = $resource( 'wp-json/posts?type=project' ).query();
				return projectsList;
			}
			
			return projects;
		}
	]);
	services.factory( 'pages', [
		'$resource',
		function( $resource )
		{
			pages = {};

			pages.getPages = function()
			{
				var pagesList = $resource( 'wp-json/posts?type=page' ).query();
				return pagesList;
			}

			return pages;
		}
	])
})();