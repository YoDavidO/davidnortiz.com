var gulp = require( 'gulp' );
var concat = require( 'gulp-concat' );
var stylus = require( 'gulp-stylus' );
var uglify = require( 'gulp-uglify' );
var mincss = require( 'gulp-minify-css' );

gulp.task( 'vendor', function(){
	gulp.src([
		'./bower_components/angular/angular.js',
		'./bower_components/angular-resource/angular-resource.js',
		'./bower_components/angular-sanitize/angular-sanitize.js',
		'./bower_components/angular-ui-router/release/angular-ui-router.js',
		'./bower_components/gsap/src/uncompressed/TweenMax.js'
	])
	.pipe( concat( 'vendor.min.js' ) )
	.pipe( gulp.dest( './scripts' ) );
});

gulp.task( 'site', function(){
	gulp.src( [ './app/**/*.js', './app/app.js' ] )
		.pipe( concat( 'site.min.js' ) )
		.pipe( gulp.dest( './scripts' ) );
});

gulp.task( 'stylus', function(){
	gulp.src( ['./app/style.styl' ] )
		.pipe( stylus() )
		.pipe( concat( 'style.css' ) )
		.pipe( gulp.dest( './' ) );
});

gulp.task( 'partials', function(){
	gulp.src( [
			'./app/projects/**.html',
	        './app/profile/**.html',
	        './app/contact/**.html',
	        './app/intro/**.html',
	        './app/layout/**.html'
		])
		.pipe( gulp.dest( './partials' ) );
});

gulp.task( 'watch', function(){
	gulp.watch( './app/**/*.js', [ 'site' ] );
	gulp.watch( './app/**/*.styl', [ 'stylus' ] );
	gulp.watch( './app/**/*.html', [ 'partials' ] );
});

gulp.task( 'default', [ 'site', 'vendor', 'stylus', 'partials', 'watch' ] );