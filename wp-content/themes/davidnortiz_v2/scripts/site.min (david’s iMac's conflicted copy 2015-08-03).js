(function(){
	app = angular.module( 'DavidOrtiz', [
        'ngResource',
        'ngSanitize',
        'ui.router',
        'DavidOrtiz.services',
        'DavidOrtiz.directives',
        'DavidOrtiz.filters',
		'DavidOrtiz.layout',
        'DavidOrtiz.intro',
		'DavidOrtiz.projects',
        'DavidOrtiz.projectDetails',
		'DavidOrtiz.profile',
		'DavidOrtiz.contact'
	])
	.config([
		'$stateProvider', 
        '$urlRouterProvider',
		function ($stateProvider, $urlRouterProvider) 
		{
            $urlRouterProvider.otherwise( function($injector){
                $state = $injector.get( '$state' );
                $state.go( 'projects' );
            } )

			$stateProvider
                .state( 'projects', {
                    url : '/projects',
                    templateUrl : myLocalized.partials + '/projects.html',
                    params : {
                        sectionTitle : 'PROJECTS'
                    }
                })
                .state( 'projects_direct', {
                    url : '/projects/:id',
                    templateUrl : myLocalized.partials + '/projectDetails.html',
                    params : {
                        sectionTitle : 'PROJECTS'
                    }
                })
                .state( 'profile', {
                    url : '/profile',
                    templateUrl : myLocalized.partials + '/profile.html',
                    params : {
                        sectionTitle : 'PROFILE'
                    }
                })
                .state( 'contact', {
                    url : '/contact',
                    templateUrl : myLocalized.partials + '/contact.html',
                    params : {
                        sectionTitle : 'CONTACT'
                    }
                });
		}
	]);
})();

(function(){
	
	directives = angular.module('DavidOrtiz.directives', [] );

	directives.directive('loadingSection', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                iElement.hide();
                TweenMax.to( iElement, 0, { opacity : 0 } );
                $rootScope.$on( '$viewContentLoaded', function(e){
                    iElement.show();
                    TweenMax.to( iElement, 0.35, { opacity : 1, ease : Quad.easeOut } );
                } );
            }
        };
    }]);

    directives.directive('loadingImg', ['$rootScope', function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                var $superParent = iElement.parent().parent().parent();
                $superParent.hide();
                TweenMax.to( $superParent, 0, { opacity : 0 } );
                iElement.on( 'load', function(e){
                    $superParent.show();
                    TweenMax.to( $superParent, 1, { opacity : 1, ease : Expo.easeInOut} );
                });
            }
        };
    }]);

    directives.directive('mNav', [function () {
        return {
            restrict: 'E',
            link: function (scope, iElement, iAttrs) {
                var toggles = iElement.find('button');
                var mMenu = iElement.find( '.mnav' );
                var menuOpen = false;
                var navBtns = iElement.find( 'a' );

                TweenMax.to( mMenu, 0, { opacity : 0 } );
                toggles.on( "click", function(e) {
                    e.preventDefault();
                    (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
                    toggler();
                });

                navBtns.on( 'click', function( e ){
                    toggles.removeClass( 'is-active' );
                    menuOpen = false;
                    TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                        mMenu.hide();
                    } } );

                    navBtns.removeClass( 'mCurrentSection' );
                    jQuery( e.currentTarget ).addClass('mCurrentSection');
                });

                function toggler()
                {
                    if( menuOpen )
                    {
                        menuOpen = false;
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                    else
                    {
                        menuOpen = true;
                        mMenu.show();
                        TweenMax.to( mMenu, .5, { opacity : 1, ease : Expo.easeOut, overwrite : 'all' } );
                    }
                }

                iAttrs.$observe( 'closeMenu', function( res ){
                    if( menuOpen )
                    {
                        menuOpen = false;
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                } );

                var win = jQuery( window );
                win.on( 'resize', function(){
                    if( win.width() > 960 && menuOpen )
                    {
                        menuOpen = false;
                        toggles.removeClass( 'is-active' );
                        TweenMax.to( mMenu, .5, { opacity : 0, ease : Expo.easeOut, overwrite : 'all', onComplete : function(){
                            mMenu.hide();
                        } } );
                    }
                })
            },
            templateUrl : myLocalized.partials + "/mNav.html"
        };
    }]);

    directives.directive('btnOverer', [function () {
        return {
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                var out = iElement.find( '.contactBtn' );
                var over = iElement.find( '.contactBtn_over' );

                over.show();
                TweenMax.to( over, 0, { opacity : 0 } );

                iElement.on( 'mouseenter', function(e){
                    TweenMax.to( over, 0.35, { opacity : 1, ease : Quad.easeOut, overwrite : 'all' } );
                });
                iElement.on( 'mouseout', function(e){
                    TweenMax.to( over, 0.35, { opacity : 0, ease : Quad.easeOut, overwrite : 'all' } );
                });
            }
        };
    }]);
})();







(function(){

	filters = angular.module('DavidOrtiz.filters', []);

	filters.filter( 'htmlToPlainText', function(){
		return function( text ){
			return String(text).replace(/<[^>]+>/gm, '').trim();
		}
	})

})();
(function(){

	services = angular.module('DavidOrtiz.services', []);

	services.factory('projectsList', [
		'$resource',
		function ( $resource ) 
		{
			projects = {};

			projects.getProjects = function()
			{
				var projectsList = $resource( 'wp-json/posts?type=project' ).query();
				return projectsList;
			}
			
			return projects;
		}
	]);
	services.factory( 'pages', [
		'$resource',
		function( $resource )
		{
			pages = {};

			pages.getPages = function()
			{
				var pagesList = $resource( 'wp-json/posts?type=page' ).query();
				return pagesList;
			}

			return pages;
		}
	])
})();
( function(){

	angular.module('DavidOrtiz.layout', [] )
		.controller('LayoutCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			layoutController
		] );

	function layoutController( $rootScope, $scope, $timeout )
	{
		var layout = this;
		layout.navLogo = myLocalized.images + 'pastelLogo.png';
        layout.dNav = myLocalized.partials = '/nav.html';

        $rootScope.$on( '$stateChangeStart', function(e, to){
        	$timeout(function(){
				layout.currentSection = to.name;
				if( layout.closeMenu )
					layout.closeMenu = false;
				else
					layout.closeMenu = true;

				if( layout.currentSection === 'contact' )
					layout.hideFooter = true;
				else
					layout.hideFooter = false;
			});
        });
		
	}

})();
( function(){

	angular.module('DavidOrtiz.profile', [ 'DavidOrtiz.services', 'ngSanitize' ] )
		.controller('ProfileCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'pages',
			profileController
		] );

	function profileController( $rootScope, $scope, $timeout, $stateParams, pages )
	{
		var me = this;

		me.profileImg = myLocalized.images + '/profilePic.png'; 

		pages.getPages().$promise.then( function( res ){
			$timeout( function(){
				me.profileBody = res[0].content;
			} )
		});
	}

})();
(function(){
	angular.module( 'DavidOrtiz.intro', [] )
		.controller( 'IntroCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			introController
		]);

	function introController( $rootScope, $scope, $timeout, $stateParams )
	{
		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});
	}
})();
(function(){

	angular.module( 'DavidOrtiz.projectDetails', [ 'DavidOrtiz.services' ] )
		.controller('ProjectDetailsCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'$filter',
			'projectsList',
			projectDetailsController
		])
		.controller('MProjectDetailsCtrl', [
			'$rootScope',
			'$scope',
			'$timeout',
			'$stateParams',
			'$filter',
			'projectsList',
			mProjectDetailsController
		])
		.directive( 'detailOpener', [ function(){
			return {
				restrict : 'A',
				link : function( scope, element, attrs )
				{
					var detailsOpened = false;

					element.bind( 'click', function( e ){
						e.preventDefault();

						if( detailsOpened )
						{
							detailsOpened = false;
							element.removeClass('open');
						}
						else
						{
							detailsOpened = true;
							element.addClass('open');
						}
					})
				}
			};
		}]);


	function projectDetailsController( $rootScope, $scope, $timeout, $stateParams, $filter, projectsList )
	{
		var pdcScope = this;
		pdcScope.project = {};

		pdcScope.detailsOpen = myLocalized.images + 'leftArrow.png';

		projectsList.getProjects().$promise.then( function( res ){
			$timeout( function(){
				pdcScope.project = $filter('filter')( res, { ID : parseInt( $stateParams.id ) } )[0];
				pdcScope.primaryImage = pdcScope.project.acf.project_images[0].url;

				if( pdcScope.project.acf.project_link === "" )
					pdcScope.noLink = true;
				else
					pdcScope.noLink = false;

				$rootScope.currentSection = $stateParams.sectionTitle;
			});
		});
	}


	function mProjectDetailsController( $rootScope, $scope, $timeout, $stateParams, $filter, projectsList )
	{
		$scope.project = {};
		projectsList.getProjects().$promise.then( function( res ){
			$timeout( function(){
				$scope.project = $filter( 'filter' )( res, { ID : parseInt( $stateParams.id ) } )[0];
			});
		});
		$rootScope.$on( '$stateChangeStart', function( e, to ){
			projectsList.getProjects().$promise.then( function( res ){
				$timeout( function(){
					$scope.project = $filter( 'filter' )( res, { ID : parseInt( $stateParams.id ) } )[0];
				});
			});
		});
		
	}

})();
















( function(){

	angular.module('DavidOrtiz.projects', [ 'DavidOrtiz.services' ] )
		.controller('ProjectsCtrl', [
			'$rootScope',
			'$scope', 
			'$timeout',
			'$stateParams',
			'projectsList',
			projectsController
		] )
		.directive('projectTile', [function () {
			return { restrict: 'A', link: projectTileDirective };
		}]);

	function projectsController( $rootScope, $scope, $timeout, $stateParams, projectsList )
	{
		var projectsScope = this;
		projectsScope.projects = [];
		projectsList.getProjects().$promise.then( function( res ){
			for( var i = 0; i < res.length; i++ )
			{
				projectsScope.projects.push( res[i] );
			}
		});

		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});

	}

	function projectTileDirective( scope, element, attr )
	{
		var win = jQuery( window );
		element.bind( 'mouseover', function( e ){
			TweenMax.to( element.find( '.projectTileOverlay' ), .35, { opacity : 1, ease : Quad.easeOut, overwrite : 'all' } );
		});
		element.bind( 'mouseout', function( e ){
			TweenMax.to( element.find( '.projectTileOverlay' ), .35, { opacity : 0, ease : Quad.easeOut } );
		});
	}

})();
( function(){

	angular.module('DavidOrtiz.projects', [ 'DavidOrtiz.services' ] )
		.controller('ProjectsCtrl', [
			'$rootScope',
			'$scope', 
			'$timeout',
			'$stateParams',
			'projectsList',
			projectsController
		] )
		.directive('projectTile', [function () {
			return { restrict: 'A', link: projectTileDirective };
		}]);

	function projectsController( $rootScope, $scope, $timeout, $stateParams, projectsList )
	{
		var projectsScope = this;
		projectsScope.projects = [];
		projectsList.getProjects().$promise.then( function( res ){
			for( var i = 0; i < res.length; i++ )
			{
				projectsScope.projects.push( res[i] );
			}
		});

		$timeout( function(){
			$rootScope.currentSection = $stateParams.sectionTitle;
		});

	}

	function projectTileDirective( scope, element, attr )
	{
		var overlay = element.find( '.projectTileOverlay' );
		overlay.show();
		TweenMax.to( overlay, 0, { opacity : 0 } );
		
		element.bind( 'mouseover', function( e ){
			TweenMax.to( overlay, .35, { opacity : 1, ease : Quad.easeOut, overwrite : 'all' } );
		});
		element.bind( 'mouseout', function( e ){
			TweenMax.to( overlay, .35, { opacity : 0, ease : Quad.easeOut } );
		});
	}

})();